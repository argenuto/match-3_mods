﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public static class Utilities
{
    /// <summary>
    /// Helper method to animate potential matches
    /// </summary>
    /// <param name="potentialMatches"></param>
    /// <returns></returns>
    public static IEnumerator AnimatePotentialMatches(IEnumerable<GameObject> potentialMatches)
    {
		
        for (float i = 1f; i >= 0.3f; i -= 0.1f)
        {
            foreach (var item in potentialMatches)
            {
                Color c = item.GetComponent<SpriteRenderer>().color;
                c.a = i;
                item.GetComponent<SpriteRenderer>().color = c;
            }
            yield return new WaitForSeconds(Constants.OpacityAnimationFrameDelay);
        }
        for (float i = 0.3f; i <= 1f; i += 0.1f)
        {
            foreach (var item in potentialMatches)
            {
                Color c = item.GetComponent<SpriteRenderer>().color;
                c.a = i;
                item.GetComponent<SpriteRenderer>().color = c;
            }
            yield return new WaitForSeconds(Constants.OpacityAnimationFrameDelay);
        }
    }

    /// <summary>
    /// Checks if a shape is next to another one
    /// either horizontally or vertically
    /// </summary>
    /// <param name="s1"></param>
    /// <param name="s2"></param>
    /// <returns></returns>
    public static bool AreVerticalOrHorizontalNeighbors(Shape s1, Shape s2)
    {
        return (s1.Column == s2.Column ||
                        s1.Row == s2.Row)
                        && Mathf.Abs(s1.Column - s2.Column) <= 1
                        && Mathf.Abs(s1.Row - s2.Row) <= 1;
    }

    /// <summary>
    /// Will check for potential matches vertically and horizontally
    /// </summary>
    /// <returns></returns>
	public static IEnumerable<GameObject> checkPotentialsSimplified (ShapesArray shapes)
	{
		List<List<GameObject>> matches = new List<List<GameObject>> ();
		//we put the "pointer" in just the middle of the arrayshape
		int center_y = UnityEngine.Random.Range(0,Constants.Rows);
		int center_x = UnityEngine.Random.Range(0,Constants.Columns);

		//we check the shapearray in spiral way
		int radius = 0, i = 0;
		int pointer_col = center_x;
		int pointer_row = center_y;
		while (radius <= (int)Math.Max (Constants.Rows, Constants.Columns)) {
			do {
				
				//using trigonometrical ecuations we select the next shape
				if (radius > 0) {
					pointer_col = center_x + Mathf.RoundToInt (radius * Mathf.Cos (i * Mathf.PI / (radius * 4)));
					pointer_row = center_y + Mathf.RoundToInt (radius * Mathf.Sin (i * Mathf.PI / (radius * 4)));
				}	
//				Debug.Log ("pointer:"+pointer_col + " " + pointer_row);
				//if the pointer is out of range search another one
				if (pointer_col > 0 && pointer_row > 0 && pointer_col < Constants.Columns && pointer_row < Constants.Rows) {
					GameObject pointer_shape = shapes [pointer_row, pointer_col];
					//looking in all the neighbours of the pointer shape. horizontal,vertical and diagonals
					for (int j = 0; j < 8; j++) {
						float local_cos = Mathf.Cos (j * Mathf.PI / 4);
						float local_sin = Mathf.Sin (j * Mathf.PI / 4);
						int local_pntr_col = pointer_col + Mathf.RoundToInt (local_cos);
						int local_pntr_row = pointer_row + Mathf.RoundToInt (local_sin);
						GameObject nxt_shape;
						//if there is not neighbour nearby, search another one 
						if (local_pntr_col > 0 && local_pntr_row > 0 && local_pntr_col < Constants.Columns && local_pntr_row < Constants.Rows) {
							GameObject local_shape = shapes [local_pntr_row, local_pntr_col];
							//Debug.Log (local_pntr_col + " " + local_pntr_row);
							int diag_shape_col, diag_shape_row;
							if (pointer_shape.GetComponent<Shape> ().IsSameType (local_shape.GetComponent<Shape> ())) {
								// we look 45 degrees over and below
								for (int k = 0; k < 2; k++) {
									int diag_modifier_x = 0;
									int diag_modifier_y = 0;
									switch (j) {

									case 0:
										diag_modifier_x =	1;
										diag_modifier_y =	Mathf.RoundToInt (Mathf.Pow (-1, k));
										break;
									case 1:
										diag_modifier_x =	Mathf.RoundToInt (Mathf.Pow (-1, k));
										diag_modifier_y =	-1 * Mathf.RoundToInt (Mathf.Pow (-1, k));
										break;
									case 2:
										diag_modifier_y =	1;
										diag_modifier_x =	Mathf.RoundToInt (Mathf.Pow (-1, k));
										break;
									case 3:
										diag_modifier_y =	Mathf.RoundToInt (Mathf.Pow (-1, k));
										diag_modifier_x =	Mathf.RoundToInt (Mathf.Pow (-1, k));
										break;
									case 4:
										diag_modifier_x =	-1;
										diag_modifier_y =	Mathf.RoundToInt (Mathf.Pow (-1, k));
										;
										break;
									case 5:
										diag_modifier_x =	Mathf.RoundToInt (Mathf.Pow (-1, k));
										diag_modifier_y =	-Mathf.RoundToInt (Mathf.Pow (-1, k));
										break;
									case 6:
										diag_modifier_y =	-1;
										diag_modifier_x =	Mathf.RoundToInt (Mathf.Pow (-1, k));
										break;
									case 7:
										diag_modifier_y =	Mathf.RoundToInt (Mathf.Pow (-1, k));
										diag_modifier_x =	Mathf.RoundToInt (Mathf.Pow (-1, k));
										break;

									}

									diag_shape_col = local_pntr_col + diag_modifier_x;
									diag_shape_row = local_pntr_row + diag_modifier_y;
									//if there si not next shape check the next one
									if (diag_shape_col > 0 && diag_shape_row > 0 && diag_shape_col < Constants.Columns && diag_shape_row < Constants.Rows) {
										nxt_shape = shapes [diag_shape_row, diag_shape_col];
										if (local_shape.GetComponent<Shape> ().IsSameType (nxt_shape.GetComponent<Shape> ()))
											matches.Add (new List<GameObject> (){ pointer_shape, local_shape, nxt_shape });
													
									}
								}

							}
						}
					}
				}
				if (matches.Count >= 3){
					Debug.Log("Tuve que dar "+i+" vueltas para conseguirlas");
					return matches [UnityEngine.Random.Range (0, matches.Count - 1)];}

			
				if (radius >= (int)Math.Max (Constants.Rows, Constants.Columns) / 2 && matches.Count > 0 && matches.Count <= 2)
				{Debug.Log("Tuve que dar "+i+" vueltas para conseguirlas");
					return matches [UnityEngine.Random.Range (0, matches.Count - 1)];}

				i++;
			} while(i < 2 * radius * 4);
			radius++;
			i = 0;
		
		}


		Debug.Log("paaanaaaa");

		return null;
	}
}


